#!/bin/bash

function get_branches() {
    git fetch --all --prune --quiet
    echo -e "$(git branch --all | grep remotes | awk -F\/ '{print $NF}' | sort -u)"
}

function prove_branch() {
    local branch=$1
    local log=$2

    if [ -e .mycatalyst ] ; then
        ./zs_prove t/998-kill-test_server.t &>/dev/null
    fi
    if [ -e .mytest ] ; then
        ./zs_prove t/999-cleanup.t &>/dev/null
    fi

    cp etc/zaaksysteem.conf.dist etc/zaaksysteem.conf

    echo "--- prove output:" >> $log
    ./zs_prove -j 2 &>> $log
}

function cover_branch() {
    local branch=$1
    local loc=$2
    local today=$(date +%Y%m%d)

    ./dev-bin/coverage.sh
    rm -rf cover_db/{runs,structure,cover.13}
    mv cover_db $loc/$branch.$today
}

function init_log() {
    local branch=$1
    local dir=$2

    local log=$dir/$branch.log.$(date +%Y%m%d.%H%M%S)
    local msg="Testreport for $branch [$log]";
    echo $msg >> $log;
    echo $log
}

function git_pull() {
    local branch=$1
    local log=$2

    git checkout $branch &>/dev/null
    git pull -q

    if [ $? -ne 0 ] ; then
        echo "--- git-pull:" >> $log
        echo "Failed to pull $branch"
    fi

    echo "--- git-status:" >> $log
    git status >> $log

    echo "--- git-lastlog:" >> $log
    git log -n1 >> $log
}

function mail_log() {
    local branch=$1
    local log=$2
    local mail=$3
    local now=$(date +"%Y%m%d.%H:%M")

    local subject="Tests for %s %s (%s)"
    local success="SUCCEEDED"

    grep -q "FAIL" $log
    if [ $? -eq 0 ] ; then
        success="FAILED"
    fi
    subject=$(printf "$subject" $branch $success $now)

    cat $log | mail -s "$subject" $mail
}
