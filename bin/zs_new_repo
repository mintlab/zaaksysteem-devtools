#!/bin/bash

# Open configuration file
if [ -f "$HOME/.zsrc" ]; then
    source "$HOME/.zsrc"
else 
    read -p "You don't have a configuration file yet. Create one? (This will open an editor) (y/n) "
    [ "$REPLY" != "y" ] && exit
    [ "$REPLY" == "y" ] &&
        printf "bitbucket_username=jeusername\nbitbucket_password=\"jepassword\"\n" > "$HOME/.zsrc" &&
        ${EDITOR:="/usr/bin/vim"} "$HOME/.zsrc"
    echo 'Configuration file created. Setting permissions to read'
    chmod 600 "$HOME/.zsrc"
    echo "Permission set to 600"
    source "$HOME/.zsrc"
fi

usage()
{
    cat << EOF
usage: $(basename $0) options

Zaaksysteem Bitbucket repository creation assister.

OPTIONS:
    -h  Show this message

    -b  Branch type: feature, bugfix, style, textfix (required)

    -n  Branch name: removed_unused_download_function (required)

    -r  Repository name: the name of the repository on BitBucket (default: 'sprint-##')

    -s  Source branch: the branch that will be used as a base for the
        new branch. Commonly this will be 'quarterly' or 'master'. (default: 'quaterly')

Example:

$0 -b feature -n remove_unused_download_function

This will create a BitBucket repository by the name of 'sprint-##' with a
copy of the quarterly branch in the newly created branch remove_unused_download_function.

EOF
}

# Do some fancy schmancy default repository name magic with uneven weeknumbers
WEEK_NUMBER=$(date +%V)
WEEK_EVEN=$[ $WEEK_NUMBER % 2 ]

if [ $WEEK_EVEN -eq "0" ]
then
    # Use padding, lest we run into natural sorting errors when listing early-in-the-year sprint repos
    REPO_NAME=$(printf "sprint-%02d" $(expr $WEEK_NUMBER - 1))
else
    REPO_NAME=$(printf "sprint-%02d" $WEEK_NUMBER)
fi

BRANCH_TYPE=
BRANCH_NAME=
SOURCE_BRANCH=quaterly # Only if you have a good reason not to should this be used as the base of the branch

while getopts "hs:b:n:r:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         b)
             BRANCH_TYPE=$OPTARG
             ;;
         r)
             REPO_NAME=$OPTARG
             ;;
         n)
             BRANCH_NAME=$OPTARG
             ;;
         s)
             SOURCE_BRANCH=$OPTARG
             ;;
     esac
done

cat << EOF
    Branch type: ${BRANCH_TYPE:="? (required)"}
    Branch name: ${BRANCH_NAME:="? (required)"}
    Fork repository name: zaaksysteem-${REPO_NAME}
    Source branch: mintlab/zaaksysteem ${SOURCE_BRANCH}

EOF

if [[ -z $BRANCH_TYPE ]] || [[ -z $BRANCH_NAME ]] || [[ -z $SOURCE_BRANCH ]] || [[ -z $REPO_NAME ]]
then
    usage
    exit 1
fi

REPO_FULLNAME="zaaksysteem-$REPO_NAME"
BRANCH_FULLNAME="$BRANCH_TYPE-$BRANCH_NAME"

read -p "Create repository \"$REPO_FULLNAME\" on BitBucket under user $bitbucket_username? (y/n) "
    [ "$REPLY" != "y" ] && echo 'Aborted.' && exit
    [ "$REPLY" == "y" ] && echo "Requesting fork at BitBucket" &&
        /usr/bin/curl --user $bitbucket_username:$bitbucket_password https://bitbucket.org/api/1.0/repositories/mintlab/zaaksysteem/fork --data "name=$REPO_FULLNAME"

read -p "Created fork, create clone in current directory? (y/n) "
    [ "$REPLY" != "y" ] && echo 'Done.' && exit
    [ "$REPLY" == "y" ] && echo "Creating clone.." &&
        git clone -b $SOURCE_BRANCH git@bitbucket.org:$bitbucket_username/$REPO_FULLNAME.git

read -p "Created clone, create branch copy of $SOURCE_BRANCH to $BRANCH_FULLNAME?  (y/n) "
    [ "$REPLY" != "y" ] && echo 'Done.' && exit
    [ "$REPLY" == "y" ] && echo "Copying branch.." && cd $REPO_FULLNAME && git branch $BRANCH_FULLNAME && git checkout $BRANCH_FULLNAME 
