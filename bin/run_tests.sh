#!/bin/bash

SELF=$(basename $0);
BASEDIR=$(readlink -m $(dirname $0)/../)
LOCK=/tmp/zs_tests.lock
NO_CRON=/tmp/zs_tests.nocron

rm_lock() {
    rm -f $LOCK
}

trap 'rm_lock; exit' SIGHUP SIGINT SIGTERM

DEF_EMAIL=bo@mintlab.nl

if [ -z $ZS_PROVE ] ; then
    EMAIL=$DEF_EMAIL
else
    EMAIL=$ZS_PROVE
fi

source $BASEDIR/lib/zaaksysteem.lib.sh

function usage() {
    local rc=$1
    [ -z $rc ] && rc=0
    echo "Usage: $SELF -z /path/to/zaaksysteem -l /path/to/log [ -p ] [ -c /path/to/basedir ] [ -h ] [ -e email@address ] branch [ [ branch ] [ branch ] ... [ branch ] ]"
    echo ""
    echo "OPTIONS:"
    echo "-p: Run prove"
    echo "-c: Basedir for the coverage file, we append the branch name"
    echo "-z: Location of the zaaksysteem git repo"
    echo "-l: Logdir"
    echo "-e: Your mail address, defaults to $DEF_EMAIL. You can also set the ZS_PROVE=email@adres.nl env variable"
    echo "-x: Don't run if the lastlog is the same as the previous run"
    echo "-h: This help"
    exit $rc
}

while getopts "z:l:gpc:e:hx" name
do
    case $name in
        z) repodir=$OPTARG ;;
        l) logdir=$OPTARG ;;
        c) coverage=$OPTARG ;;
        p) prove=1 ;;
        e) EMAIL=$OPTARG ;;
        x) lastlog=1 ;;
        h) usage ;;
        *) usage 1 ;;
    esac
done
shift $((OPTIND - 1))

if [ -z "$repodir" ] || [ -z "$logdir" ] || [ -z "$coverage" ] && [ -z "$prove" ] ; then
    usage 1
fi

if [ -f $CRON ] ; then
    exit 0;
fi

if [ ! -f $LOCK ] ; then
    touch $LOCK
else
    echo "Not running, lock file $LOCK found!"
    exit 0
fi

repodir=$(readlink -m $repodir)
logdir=$(readlink -m $logdir)

if [ -n "$coverage" ]  ; then
    coverage=$(readlink -m $coverage)
fi

for i in $repodir $logdir $coverage; do
    if [ ! -d "$i" ] || [ ! -r "$i" ] || [ ! -w "$i" ] ; then
        echo "'$i' does not exist or is not readable or writeable!";
        exit 1
    fi
done

_PWD=$(pwd)

cd $repodir
all_branches="$(get_branches)"

if [ -n "$1" ] ; then
    for i in $@ ; do
        z=$(echo -e "$all_branches" | grep -- $i)
        if [ $? -eq 0 ] ; then
            branches="$branches $z"
        else
            echo "Branch $i does not exist!" >&2
        fi
    done
    if [ -z "$branches" ] ; then
        echo "No branches selected, aborting!" >&2
        exit 2
    fi
else
    branches=$all_branches
fi

for branch in $branches ; do
    logfile=$(init_log $branch $logdir)

    git_pull $branch $logfile

    current_commit=$(git log -n1 | grep commit | awk '{print $NF}')
    lc=$logdir/$branch.commit
    if [ -n "$lastlog" ] ; then
        touch $lc
        last_commit=$(cat $lc);
        if [ "$last_commit" == "$current_commit" ] ; then
            rm $logfile
            continue
        fi
    fi

    if [ -n "$prove" ] ; then
        prove_branch $branch $logfile
        mail_log $branch $logfile $EMAIL
    fi

    [ -n "$coverage" ] && cover_branch $branch $coverage
    echo $current_commit > $lc

done

rm_lock

cd $_PWD
